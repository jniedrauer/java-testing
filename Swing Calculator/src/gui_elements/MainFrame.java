package gui_elements;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;

public class MainFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7812210082011168004L;
	
	static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	static Dimension appsize = new Dimension(290,300);
	static int startx = (int)screenSize.getWidth()/2 - (int)appsize.getWidth()/2;
	static int starty = (int)screenSize.getHeight()/2 - (int)appsize.getHeight()/2;
	
	private static void showMainUI() {
		JFrame f = new JFrame("Swing Calculator");
		f.setPreferredSize(appsize);
		f.setMinimumSize(appsize);
		f.setLocation(startx,starty);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Define contents
		JPanel contentPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(5,5,5,5);
		
		//Top entry box and output
		JTextField entryBox = new JTextField(25);
	    PlainDocument doc = (PlainDocument) entryBox.getDocument();
	    doc.setDocumentFilter(new inputFilter());
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = 5;
		c.weightx = 1;
		c.weighty = 1;
		c.gridx = 0;
		c.gridy = 0;
		contentPanel.add(entryBox, c);
		
		//Array of buttons
		String[][] buttonLabel = new String[][] {
			{"7","8","9","÷","<"},
			{"4","5","6","×","x²"},
			{"1","2","3","-","√"},
			{"0",".","%","+","="}
		};
		JButton[][] button = new JButton[buttonLabel.length][buttonLabel[0].length];
		c.gridwidth = 1;
		for (int i=0; i<buttonLabel.length; i++) {
		     for (int j=0; j<buttonLabel[i].length; j++) {
		        button[i][j] = new JButton();
		        button[i][j].setText(buttonLabel[i][j]);
		        c.gridx = j;
		        c.gridy = i+1;
		        contentPanel.add(button[i][j], c);
		     }

		}
		
		f.setContentPane(contentPanel);
		f.pack();
		f.setVisible(true);
	}
	
	public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                showMainUI();
            }
        });
	}
}

class inputFilter extends DocumentFilter {
}
